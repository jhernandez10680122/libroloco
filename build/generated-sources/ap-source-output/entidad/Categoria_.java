package entidad;

import entidad.Libro;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2014-11-30T23:32:34")
@StaticMetamodel(Categoria.class)
public class Categoria_ { 

    public static volatile SingularAttribute<Categoria, Short> id;
    public static volatile SingularAttribute<Categoria, String> nombreImg;
    public static volatile SingularAttribute<Categoria, String> nombre;
    public static volatile CollectionAttribute<Categoria, Libro> libroCollection;

}