package entidad;

import entidad.Categoria;
import entidad.Ordenlibro;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2014-11-30T23:32:34")
@StaticMetamodel(Libro.class)
public class Libro_ { 

    public static volatile SingularAttribute<Libro, String> descripcion;
    public static volatile SingularAttribute<Libro, Date> ultimoActual;
    public static volatile CollectionAttribute<Libro, Ordenlibro> ordenlibroCollection;
    public static volatile SingularAttribute<Libro, BigDecimal> precio;
    public static volatile SingularAttribute<Libro, String> titulo;
    public static volatile SingularAttribute<Libro, Integer> id;
    public static volatile SingularAttribute<Libro, String> nombreImg;
    public static volatile SingularAttribute<Libro, String> autor;
    public static volatile SingularAttribute<Libro, Categoria> categoriaId;

}