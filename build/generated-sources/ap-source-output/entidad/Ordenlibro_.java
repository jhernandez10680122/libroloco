package entidad;

import entidad.Libro;
import entidad.Orden;
import entidad.OrdenlibroPK;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2014-11-30T23:32:34")
@StaticMetamodel(Ordenlibro.class)
public class Ordenlibro_ { 

    public static volatile SingularAttribute<Ordenlibro, Libro> libro;
    public static volatile SingularAttribute<Ordenlibro, OrdenlibroPK> ordenlibroPK;
    public static volatile SingularAttribute<Ordenlibro, Short> cantidad;
    public static volatile SingularAttribute<Ordenlibro, Orden> orden;

}