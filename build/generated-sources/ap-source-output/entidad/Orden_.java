package entidad;

import entidad.Cliente;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2014-11-30T23:32:34")
@StaticMetamodel(Orden.class)
public class Orden_ { 

    public static volatile SingularAttribute<Orden, BigDecimal> total;
    public static volatile SingularAttribute<Orden, Cliente> clienteId;
    public static volatile SingularAttribute<Orden, Date> fechaCreacion;
    public static volatile SingularAttribute<Orden, Integer> id;
    public static volatile SingularAttribute<Orden, Integer> numConfirmacion;

}