/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package control;

import carro.Carro;
import entidad.Categoria;
import entidad.Libro;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import sesion.CategoriaFacade;
import sesion.OrdenAdmin;
import sesion.LibroFacade;
import validacion.Validaciones;

/**
 *
 * @author Daftzero
 */
@WebServlet(name = "Controller",
            loadOnStartup = 1,
            urlPatterns = {"/categoria",
                           "/agregaACarro",
                           "/vercarro",
                           "/actualizaCarro",
                           "/pago",
                           "/hacerpago"})
public class Control extends HttpServlet {

    private String cargoExtra;

    @EJB
    private CategoriaFacade categoriaFacade;
    @EJB
    private LibroFacade libroFacade;
    @EJB
    private OrdenAdmin OrdenAdmin;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {

        super.init(servletConfig);

        cargoExtra = servletConfig.getServletContext().getInitParameter("cargoDeEnvio");

        getServletContext().setAttribute("categorias", categoriaFacade.findAll());
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String userPath = request.getServletPath();
        HttpSession session = request.getSession();
        Categoria categoSel;
        Collection<Libro> libros;

        if (userPath.equals("/categoria")) {

            String categoriaId = request.getQueryString();

            if (categoriaId != null) {

                categoSel = categoriaFacade.find(Short.parseShort(categoriaId));

                session.setAttribute("categoSel", categoSel);

                libros = categoSel.getLibroCollection();

                session.setAttribute("categoriaLibros", libros);
            }

        } else if (userPath.equals("/vercarro")) {

            String clear = request.getParameter("clear");

            if ((clear != null) && clear.equals("true")) {

                Carro carro = (Carro) session.getAttribute("carro");
                carro.clear();
            }

            userPath = "/carro";


        } else if (userPath.equals("/pago")) {

            Carro carro = (Carro) session.getAttribute("carro");

            carro.calculateTotal(cargoExtra);


        } 


        String url = "/WEB-INF/view" + userPath + ".jsp";

        try {
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");  

        String userPath = request.getServletPath();
        HttpSession session = request.getSession();
        Carro carro = (Carro) session.getAttribute("carro");
        Validaciones validator = new Validaciones();


        if (userPath.equals("/agregaACarro")) {


            if (carro == null) {

                carro = new Carro();
                session.setAttribute("carro", carro);
            }

            String libroId = request.getParameter("libroId");

            if (!libroId.isEmpty()) {

                Libro libro = libroFacade.find(Integer.parseInt(libroId));
                carro.agregaItem(libro);
            }

            userPath = "/categoria";


        } else if (userPath.equals("/actualizaCarro")) {


            String libroId = request.getParameter("libroId");
            String cantidad = request.getParameter("cantidad");

            boolean entradaInvalida = validator.ValidaCantidad(libroId, cantidad);

                if (!entradaInvalida) {

                Libro libro = libroFacade.find(Integer.parseInt(libroId));
                carro.actualiza(libro, cantidad);
            }

            userPath = "/carro";


        } else if (userPath.equals("/hacerpago")) {

            if (carro != null) {
                System.out.println("estoy aqui en hacerpago");   
                String nombre = request.getParameter("name");
                String email = request.getParameter("email");
                String tel = request.getParameter("telefono");
                String dir = request.getParameter("direccion");
                String cp = request.getParameter("cp");
                String tjNum = request.getParameter("creditcard");

                boolean error = false;
                System.out.println("voy a ver el error"); 
                error = validator.ValidaFormulario(nombre, email, tel, dir, cp, tjNum, request);

                if (error == true) {
                    System.out.println("sigo en pago");  
                    request.setAttribute("validationErrorFlag", error);
                    userPath = "/pago";

                } else {
                     System.out.println("algo en admin");
                    int ordenId = OrdenAdmin.placeOrden(nombre, email, tel, dir, cp, tjNum, carro);
                    System.out.println(ordenId);
                    if (ordenId != 0) {
                        System.out.println("algo debe de hacer");
                        carro = null;

                        session.invalidate();

                        Map ordenMap = OrdenAdmin.getOrdenDetalles(ordenId);

                        request.setAttribute("cliente", ordenMap.get("cliente"));
                        request.setAttribute("libros", ordenMap.get("libros"));
                        request.setAttribute("regOrden", ordenMap.get("regOrden"));
                        request.setAttribute("ordenedLibros", ordenMap.get("ordenedLibros"));
                        System.out.println("se va confirmacion");
                        userPath = "/confirmacion";

                    } else {
                        userPath = "/pago";
                        System.out.println("regreso al pago");
                        request.setAttribute("ordenFail", true);
                    }
                }
            }
        }

        String url = "/WEB-INF/view" + userPath + ".jsp";

        try {
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}